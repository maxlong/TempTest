package com.maxlong.study;

/**
 * @author 作者 maxlong:
 * @version 创建时间：2017/4/24 18:07
 * 类说明
 */
public enum DatabaseType {
    ACQ,
    PAY,
    FIN;
}
