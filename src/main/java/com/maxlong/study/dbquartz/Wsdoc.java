package com.maxlong.study.dbquartz;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "WSDOC", schema = "")
public class Wsdoc {

    private Long id;
    // 设置trigger名称
    private String triggername;
    //设置表达式
    private String cronexpression;
    // 设置Job名称
    private String jobdetailname;
    //任务类名
    private String targetobject;
    //类名对应的方法名
    private String methodname;
    //设置是否并发启动任务 0是false 非0是true
    private String concurrent;
    // 如果计划任务不存则为1 存在则为0
    private String state;
    private String readme;
    //是否是已经存在的springBean 1是  0 否
    private String isspringbean;
    /**
     * 预留字段1
     */
    private String reserved1;
    /**
     * 预留字段2
     */
    private String reserved2;
    /**
     * 预留字段3
     */
    private String reserved3;
    /**
     * 预留字段4
     */
    private String reserved4;
    /**
     * 预留字段5
     */
    private String reserved5;
    /**
     * 预留字段6
     */
    private String reserved6;
    /**
     * 预留字段7
     */
    private String reserved7;
    /**
     * 预留字段8
     */
    private String reserved8;
    /**
     * 预留字段9
     */
    private String reserved9;
    /**
     * 预留字段10
     */
    private String reserved10;
    // Constructors

    /**
     * default constructor
     */
    public Wsdoc() {
    }

    /**
     * full constructor
     */
    public Wsdoc(String triggername, String cronexpression,
                 String jobdetailname, String targetobject, String methodname,
                 String concurrent, String state, String readme, String isspringbean) {
        this.triggername = triggername;
        this.cronexpression = cronexpression;
        this.jobdetailname = jobdetailname;
        this.targetobject = targetobject;
        this.methodname = methodname;
        this.concurrent = concurrent;
        this.state = state;
        this.readme = readme;
        this.isspringbean = isspringbean;
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="WSDOC_ID") //如果为oracle则可以创建一个序列，便于插入数据用
    //@SequenceGenerator(initialValue=0,name="WSDOC_ID",sequenceName="WSDOC_ID",allocationSize=1)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "TRIGGERNAME", nullable = false, length = 100)
    public String getTriggername() {
        return this.triggername;
    }

    public void setTriggername(String triggername) {
        this.triggername = triggername;
    }

    @Column(name = "CRONEXPRESSION", nullable = false, length = 100)
    public String getCronexpression() {
        return this.cronexpression;
    }

    public void setCronexpression(String cronexpression) {
        this.cronexpression = cronexpression;
    }

    @Column(name = "JOBDETAILNAME", nullable = false, length = 100)
    public String getJobdetailname() {
        return this.jobdetailname;
    }

    public void setJobdetailname(String jobdetailname) {
        this.jobdetailname = jobdetailname;
    }

    @Column(name = "TARGETOBJECT", nullable = false, length = 100)
    public String getTargetobject() {
        return this.targetobject;
    }

    public void setTargetobject(String targetobject) {
        this.targetobject = targetobject;
    }

    @Column(name = "METHODNAME", nullable = false, length = 100)
    public String getMethodname() {
        return this.methodname;
    }

    public void setMethodname(String methodname) {
        this.methodname = methodname;
    }

    @Column(name = "CONCURRENT", nullable = false, length = 100)
    public String getConcurrent() {
        return this.concurrent;
    }

    public void setConcurrent(String concurrent) {
        this.concurrent = concurrent;
    }

    @Column(name = "STATE", nullable = false, length = 100)
    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Column(name = "README", nullable = false, length = 100)
    public String getReadme() {
        return this.readme;
    }

    public void setReadme(String readme) {
        this.readme = readme;
    }

    @Column(name = "ISSPRINGBEAN", nullable = false, length = 100)
    public String getIsspringbean() {
        return isspringbean;
    }

    public void setIsspringbean(String isspringbean) {
        this.isspringbean = isspringbean;
    }

    @Column(name = "RESERVED_1", length = 100)
    public String getReserved1() {
        return reserved1;
    }

    public void setReserved1(String reserved1) {
        this.reserved1 = reserved1;
    }

    @Column(name = "RESERVED_2", length = 100)
    public String getReserved2() {
        return reserved2;
    }

    public void setReserved2(String reserved2) {
        this.reserved2 = reserved2;
    }

    @Column(name = "RESERVED_3", length = 100)
    public String getReserved3() {
        return reserved3;
    }

    public void setReserved3(String reserved3) {
        this.reserved3 = reserved3;
    }

    @Column(name = "RESERVED_4", length = 100)
    public String getReserved4() {
        return reserved4;
    }

    public void setReserved4(String reserved4) {
        this.reserved4 = reserved4;
    }

    @Column(name = "RESERVED_5", length = 100)
    public String getReserved5() {
        return reserved5;
    }

    public void setReserved5(String reserved5) {
        this.reserved5 = reserved5;
    }

    @Column(name = "RESERVED_6", length = 100)
    public String getReserved6() {
        return reserved6;
    }

    public void setReserved6(String reserved6) {
        this.reserved6 = reserved6;
    }

    @Column(name = "RESERVED_7", length = 100)
    public String getReserved7() {
        return reserved7;
    }

    public void setReserved7(String reserved7) {
        this.reserved7 = reserved7;
    }

    @Column(name = "RESERVED_8", length = 100)
    public String getReserved8() {
        return reserved8;
    }

    public void setReserved8(String reserved8) {
        this.reserved8 = reserved8;
    }

    @Column(name = "RESERVED_9", length = 100)
    public String getReserved9() {
        return reserved9;
    }

    public void setReserved9(String reserved9) {
        this.reserved9 = reserved9;
    }

    @Column(name = "RESERVED_10", length = 100)
    public String getReserved10() {
        return reserved10;
    }

    public void setReserved10(String reserved10) {
        this.reserved10 = reserved10;
    }


}