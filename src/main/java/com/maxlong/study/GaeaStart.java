package com.maxlong.study;

import cn.com.sand.component.config.DynamicPropertyHelper;
import cn.com.sand.frame.gaea.common.GaeaConstants;
import cn.com.sand.frame.gaea.remoting.http.JettyServerFactory;
import cn.com.sand.frame.gaea.remoting.tcp.zookeeper.CuratorManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author 作者 maxlong:
 * @version 创建时间：2017/8/4 17:31
 * 类说明
 */
@Slf4j
public class GaeaStart {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        context.start();
        try {
            log.info("gara server starting....");
            DynamicPropertyHelper.loadExtensionProperties("gaea");
            CuratorManager.getInstance().connect().regService(8888);
            JettyServerFactory.getInstance().start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
