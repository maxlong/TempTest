package com.maxlong.study.quartz;

import com.maxlong.study.springcache.UserInfoDao;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

/**
 * @author 作者 maxlong:
 * @version 创建时间：2017/7/7 18:03
 * 类说明
 */
@Slf4j
@Component(value = "CacheJob")
public class CacheJob {

    @Autowired
    private UserInfoDao userInfoDao;

    protected void execute() throws JobExecutionException {
        log.info("结果为空：{}", userInfoDao == null);
    }
}
