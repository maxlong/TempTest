package com.maxlong.study.tree;

/**
 * @author 作者 maxlong:
 * @version 创建时间：2018-03-07 0007 15:17
 * 类说明
 */
public class TreeNode {

    int val;

    TreeNode left, right;

    public TreeNode(int val) {
        this.val = val;
        this.left = this.right = null;
    }
}
